#!/bin/sh

/usr/sbin/libvirtd -d
/usr/sbin/virtlogd -d

# For provisionning and management networks (managed by multus), we want to bridge the VM to multus networks.
# libvirt networks won't work for that purpose because ipa agent is detecting and reporting a callback url to ironic,
# and ironic won't be able to reach the agent neither in NATed network as its address is hidden,
# nor in routed network, as we are not able to inject routes in multus networks.

# So we have to build a bridge by our own and attach it to the pod VM. We also have to manage
# IP allocation: various (multus) CNIs will set pod IPs, but nobody will care about the VMs.

# For that purpose, we use standard linux bridges (when each of the 2 VM interfaces are to be standalone/unbonded)
# or a single standard linux bridge (when the 2 interfaces will be part of a bond) instead of macvtap, as the latter would require
# to mount the whole devtmpfs in the container, as it creates a /dev/tapXX node that can not be determined in advance.

if [[ ${USE_BOND:-} == "true" ]]; then
    enslave_link_for_bond () {
        # enslave link $1 in bridge $2 and move link ip to bridge if $3
        LINK=$1
        BRIDGE=$2
        MOVE_LINK_IP_TO_BRIDGE=${3:-false}
        if ip link show $LINK &>/dev/null; then
            LINK_ADDR=$(ip -4 -o addr show $LINK | awk '{print $4}')
            ip addr del $LINK_ADDR dev $LINK
            ip link set $LINK master $BRIDGE
            if [ "$MOVE_LINK_IP_TO_BRIDGE" == "true" ]; then
                ip addr add $LINK_ADDR dev $BRIDGE
            fi
            echo "Link $LINK enslaved in bridge $BRIDGE"
        else
            echo "Link $LINK not found, it won't be enslaved in bridge $BRIDGE"
        fi
    }
    BRIDGE=provisioning
    ip link add $BRIDGE type bridge
    ip link set $BRIDGE up
    ip link set $BRIDGE type bridge vlan_filtering 1
    enslave_link_for_bond net1 $BRIDGE true
    enslave_link_for_bond net2 $BRIDGE false
    # have untagged frames received on net2 pod interface be associated with VLAN 100
    bridge vlan del dev net2 vid 1
    bridge vlan add dev net2 vid 100 pvid untagged
    SECOND_INTERFACE_BRIDGE=$BRIDGE

elif [[ ${USE_BOND:-} == "false" ]]; then
    # remove QEMU hook file added in Dockerfile
    rm /etc/libvirt/hooks/qemu
    enslave_link_for_standalone () {
        # enslave link $1 in bridge $2
        LINK=$1
        BRIDGE=$2
        if ip link show $LINK &>/dev/null; then
            ip link add $BRIDGE type bridge
            ip link set $BRIDGE up
            LINK_ADDR=$(ip -4 -o addr show $LINK | awk '{print $4}')
            ip addr del $LINK_ADDR dev $LINK
            ip link set $LINK master $BRIDGE
            ip addr add $LINK_ADDR dev $BRIDGE
            echo "Link $LINK enslaved in bridge $BRIDGE"
        else
            echo "Link $LINK not found, it won't be enslaved in bridge $BRIDGE"
        fi
    }
    enslave_link_for_standalone net1 provisioning
    enslave_link_for_standalone net2 management
    SECOND_INTERFACE_BRIDGE=management
fi

virsh pool-create storage-pool.xml

sed -i "s/NODE_MEM_GB/${NODE_MEM_GB:-4}/" dom.xml
sed -i "s/NODE_CPUS/${NODE_CPUS:-6}/" dom.xml
sed -i "s/NODE_INDEX/$(printf "%02d" $NODE_INDEX)/" dom.xml
sed -i "s/SECOND_INTERFACE_BRIDGE/${SECOND_INTERFACE_BRIDGE}/" dom.xml

virsh define dom.xml

if [[ ${ENABLE_DHCP:-} == "true" ]]; then
    if [[ ${USE_BOND:-} == "true" ]]; then
        PROVISIONING_MAC=$(virsh domiflist vbmh | awk '$3=="provisioning" {print $5}' | head -n 1)
    elif [[ ${USE_BOND:-} == "false" ]]; then
        PROVISIONING_MAC=$(virsh domiflist vbmh | awk '$3=="provisioning" {print $5}')

        # Start DHCP to allocate IP also on management network to speed up startup of ipa (even if the address won't be used at this stage)
        MANAGEMENT_MAC=$(virsh domiflist vbmh | awk '$3=="management" {print $5}')
        dnsmasq --bind-dynamic \
                --except-interface=lo \
                --interface=management \
                --dhcp-range=192.168.100.$(($NODE_INDEX+10)),192.168.100.$(($NODE_INDEX+10)),255.255.255.0 \
                --dhcp-option=option:router,192.168.100.1 \
                --dhcp-host=${MANAGEMENT_MAC},192.168.100.$(($NODE_INDEX+10)),12h \
                --dhcp-no-override \
                --dhcp-authoritative \
                --dhcp-lease-max=1
    fi
        # Start DHCP to allocate IP on provisioning network
        dnsmasq --bind-dynamic \
                --except-interface=lo \
                --interface=provisioning \
                --dhcp-range=192.168.10.$(($NODE_INDEX+10)),192.168.10.$(($NODE_INDEX+10)),255.255.255.0 \
                --dhcp-option=option:router,192.168.10.1 \
                --dhcp-host=${PROVISIONING_MAC},192.168.10.$(($NODE_INDEX+10)),12h \
                --dhcp-no-override \
                --dhcp-authoritative \
                --dhcp-lease-max=1  # make sure to do not provide IP for other VMs
fi

# Finally start sushy
sushy-emulator --config /etc/sushy/sushy-emulator.conf --debug
