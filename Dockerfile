FROM python:3-alpine as build
# hadolint ignore=DL3018, SC3020
RUN apk --no-cache update \
    && apk --no-cache upgrade \
    && apk add --no-cache build-base \
           linux-headers \
           libvirt-dev \
           openssl \
           apache2-utils \
    && pip wheel sushy_tools libvirt-python \
    && openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 -subj "/C=--/L=-/O=-/CN=sushy" -keyout key.pem -out cert.pem \
    && htpasswd -cbB htpasswd.txt admin bmcpass &> /dev/null

FROM python:3-alpine

COPY --from=build *.whl /tmp/
# hadolint ignore=DL3018
RUN apk --no-cache update \
    && apk --no-cache upgrade \
    && apk add --no-cache libvirt-daemon \
           libvirt \
           libvirt-qemu \
           libvirt-client \
           qemu-system-x86_64 \
           iproute2 \
    && rm -rf /var/cache/apk/* \
    && pip install --no-cache-dir /tmp/*.whl \
    && mkdir -p /etc/sushy

COPY --from=build key.pem cert.pem htpasswd.txt /etc/sushy/

WORKDIR /opt

COPY storage-pool.xml dom.xml entrypoint.sh ./

COPY disk-root.qcow2 image-0.qcow2
COPY disk-data.qcow2 image-1.qcow2
COPY disk-data.qcow2 image-2.qcow2

RUN mkdir -p /etc/libvirt/hooks

COPY qemu-hook.sh /etc/libvirt/hooks/qemu

COPY sushy-emulator.conf /etc/sushy/sushy-emulator.conf

CMD ["/opt/entrypoint.sh"]
