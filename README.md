# Libvirt Metal

This project builds an image that emulates baremetal in a container.

It leverages libvirt and sushy-emulator to mimic a baremetal server that can be provisioned with metal3

It is intended to be used in sylva bootstrap cluster, as a sandbox to develop or validate our current stack.

This image is intended to be deployed and used via k8s manifests defined in [charts/libvirt-metal](./charts/libvirt-metal) - see below.

## How it works

One of the challenges regarding the use of a VM inside a container is network connectivity:

The pod IP can be used by the VM if we use masquerading. However this is not usable for ironic provisioning
because ironic-python-agent is detecting and reporting its provisioning url back to ironic API.

This is why we use two additional networks provisioned by multus that will be used for provisioning and management (kubernetes api) networks.

<details>
<summary>Click to view libvirt-metal pod internals with `useBonds: true` </summary>

![libvirt-metal pod](img/libvirt-metal-pod.png)

</details>

<details>
<summary>Click to view libvirt-metal pod internals with `useBonds: false` </summary>

![libvirt-metal pod](img/libvirt-metal-pod-useBond-false.png)

</details>

> 📝 Note: The `dnsmasq` on all diagrams depends on whether `enableDHCP: true` is used or not. See more [below](#dhcp-support)

These networks are managed by multus, and attached to ironic pod, as well as libvirt-metal pods.

Each libvirt-metal pod redfish api is exposed through its own Service in (kind) host cluster, and will be reachable on following url:

```
redfish-virtualmedia://[clusterPublicIP]:[redfishPort]/redfish/v1/Systems/c0014001-b10b-f001-c0de-feeb1e54ee15
```

For now few parameters are fixed as there was no forseeable advantage to make them configurable, but this could be revisited in the future if needed:

- All the VMs share the same fixed UUID in order to have a predictable bmc & address (see above)
- BMC password is also fixed
- Bootstrap and management networks have fixed IP ranges (respectively 192.168.10.0/24 and 192.168.100.0/24)

Once provisioned, the management cluster will be accessible from its virtual IP in the management network. Unfortunately, this IP is only accessible in `kind` container and can't be reached from the host. This is why we have to expose it in kind's bootstrap cluster.

For that purpose we use a service bound to the container IP and without any pod selector, but using a static endpointSlice pointing to management cluster's virtual IP.

The `kind` container IP (the externalIP of the kubernetes-external service) is used as controlPlaneEndpoint of the cluster, so that kubernetes API will be accessible from the bootstrap and management clusters, and also from the host machine.

![bootstrap networks](img/libvirt-metal-networks.png)

Moreover, in order to make management cluster services accessible from the outside, an Ingress is configured to relay all incoming https requests targeting cluster domain to management cluster using the kubernetes-external service.

![bootstrap networks](img/libvirt-metal-mgt-access.png)

As for the cluster (baremetal) provisioning, we need to account that:
- gateway IPs (.1) for the Multus networks are configured by multus inside `kind` Docker container (following [`NetworkAttachmentDefinitions`](./charts/libvirt-metal/templates/networks-definitions.yaml)'s [host-local ipam](https://www.cni.dev/plugins/current/ipam/host-local/) `gateway`) and IP forwarding is enabled in `kind` by default, allowing for the two subnets to be routed;
- we need a small hack in `kind` to prevent masquerading in these ntworks, it's done [here](https://gitlab.com/sylva-projects/sylva-core/-/tree/1.1.0/tools/kind/systemd?ref_type=tags);
- VM trafic is only bridged in the libvirt-metal pod and will be routed in kind container;
- the only reason for which we need an IP address in the pod is to run `dnsmasq` to allocate an address to IPA in provisioning network, for it to callback to Ironic. Once the VM is provisioned, its routing will be controlled by `cluster.capm3.primary_pool_gateway` parameter.

<details>
<summary>Click to view workload cluster BM provisioning with `useBonds: true` </summary>

![BM provisioning](img/libvirt-metal-workload-cluster-provisioning.png)

</details>

<details>
<summary>Click to view workload cluster BM provisioning with `useBonds: false` </summary>

![BM provisioning](img/libvirt-metal-workload-cluster-provisioning-useBond-false.png)

</details>


## How to use

For convenience, this project includes a Helm chart that will take care of installing these components:

- Service used to reach API endpoint used to reach nester cluster deployed in libvirt-metal pods
- Multus networkAttachementDefinitions
- libvirt-metal pods definitions and associated services

Here is an example of values that can be provided to that chart:

```yaml
# Provide the IP that will be configured as controlplaneEndpoint
# for the cluster that will be deployed on emulated baremetal
clusterVirtualIp: 192.168.100.2
# Provide the IP on which the cluster will be accessible from the outside
# Usually the external address of the cluster that is hosting libvirt metal pods
clusterPublicIP: 172.18.0.2

# Define various libvirt-metal nodes that you intent to deploy
nodes:
  # First node for management-cluster, its bmh url will be:
  # redfish-virtualmedia://172.18.0.2:8000/redfish/v1/Systems/c0014001-b10b-f001-c0de-feeb1e54ee15
  # This node will be created with DHCP support and bonded interfaces (VLAN 100 as virtual interface)
  management-cp:
    memGB: 12
    numCPUs: 6
    redfishPort: 8000

  # Second node for workload cluster, its bmh url will be:
  # redfish-virtualmedia://172.18.0.2:8001/redfish/v1/Systems/c0014001-b10b-f001-c0de-feeb1e54ee15
  # This node will be created without DHCP support and using standalone/individual interfaces
  workload-cp:
    memGB: 4
    numCPUs: 2
    redfishPort: 8001
    useBond: false
    enableDHCP: false
```

This image and chart are intended to be used in sylva-core project.
In order to use it, all what you have to do is to use values files provided in that project:

```
$ ./bootstrap.sh environment-values/rke2-capm3-virt/
```

## Bonding support

By default libvirt-metal pod has a single standard Linux bridge connected to both its VM's two interfaces and the two Multus interfaces of the pod, which permits untagged traffic received from pod net1 port (Multus provisioning network) and tags with VLAN ID 100 untagged frames received on pod net2 port (Multus management network). <br/>
This way, a bond can be created out the two VM interfaces (using active-backup mode) and bonded VLAN 100 can be used as primary interface during CAPM3 Sylva deployments, with [sylva-units](https://gitlab.com/sylva-projects/sylva-core/-/tree/main/charts/sylva-units) values like:

```yaml
# see https://gitlab.com/sylva-projects/sylva-core/-/blob/main/environment-values/base-capm3-virt/capm3-virt-values.yaml
cluster:
  capm3:
    network_interfaces:
      bond_mode: "active-backup"
  control_plane:
    capm3:
      provisioning_pool_interface: bond0
      primary_pool_interface: bond0.100

    network_interfaces:
      bond0:
        type: bond
        interfaces:
          - ens4
          - ens5
        vlans:
          - id: 100
      ens4:
        type: phy
      ens5:
        type: phy
```

This is controlled globally or per node using `useBond` value. <br/>
Setting this chart value to `false` would have VM interfaces be standalone (each VM port connecting to a dedicated standard Linux bridge, connecting to a single Multus port), allowing CAPM3 deployemnts with values like:

```yaml
cluster:
  control_plane:
    capm3:
      provisioning_pool_interface: ens4
      primary_pool_interface: ens5

    network_interfaces:
      ens4:
        type: phy
      ens5:
        type: phy
```

## DHCP support

By default libvirt-metal pods are running `dnsmasq` as DHCP server. It can be useful for metal3 pre-provisioning (ie IPA image network config).
DHCP support can be disabled globally or per node using `enableDHCP` value.

## Domain configuration

In libvirt, <b>dom.xml</b> file is used to define the configuration of a domain (virtual machine or container). This file contains detailed specifications for the virtual hardware, devices, and other parameters necessary to define the behavior and characteristics of the domain.

Here's an overview of the structure of dom.xml for libvirt:
```
    Root Element:
        The root element of dom.xml is <domain>, which encapsulates the entire configuration for the domain.

    Type of Domain:
        Within the <domain> element, the <type> element specifies the type of domain, such as kvm, qemu, lxc, docker, etc. This determines the hypervisor or container technology used to run the domain.

    Name and UUID:
        The <name> element specifies the name of the domain, while the <uuid> element provides a unique identifier for the domain.

    Memory and CPU Settings:
        The <memory> element defines the amount of memory allocated to the domain, and the <vcpu> element specifies the number of virtual CPUs assigned to the domain.

    Devices:
        Various <devices> elements define the virtual hardware devices attached to the domain, such as disks (<disk>), network interfaces (<interface>), input devices (<input>), graphics devices (<graphics>), etc.

    Storage and Network Configuration:
        The <disk> elements within <devices> define the storage devices attached to the domain, including their source, target, type, and other attributes.
        <interface> elements define the network interfaces attached to the domain, specifying their type, model, MAC address, and other parameters.

    Boot Configuration:
        The <os> element specifies the operating system details and boot configuration for the domain, including the boot device (<boot>), kernel (<kernel>), initrd (<initrd>), command-line arguments (<cmdline>), etc.

    Lifecycle and Behavior:
        Various elements within <domain> may define lifecycle and behavior settings for the domain, such as CPU mode (<cpu>), scheduling policy (<sched>), resource allocation (<cputune>, <blkiotune>), etc.

    Features and Extensions:
        <features> elements may be used to enable or disable certain features or extensions for the domain, such as ACPI (<acpi>), APIC (<apic>), hypervisor-specific features (<hyperv> for Hyper-V), etc.

    Metadata and Annotations:
        Additional metadata or annotations may be included within <domain> to provide descriptive information about the domain, its purpose, or its configuration.
```

<b>Dom.xml</b> is used primarily with the virsh command-line tool or through libvirt APIs to define and manage virtual machines or containers. Once defined, <b>dom.xml</b> files are used to define, start, stop, and manage virtual machines or containers using libvirt tools and APIs.

## Disk sizing
The <b>device disk</b> section under dom.xml file allows users to define and manage virtual disks attached to virtual machines (VMs) or other virtualized environments. With libvirt's device disk, users can specify parameters such as disk format, source location, size, and other relevant attributes. This abstraction enables seamless management and manipulation of virtual storage, allowing for dynamic adjustments to the storage configuration of virtualized systems.

In our case we have:
- one <b>disk</b> section used for root partition based on disk-root.qcow2
- two <b>disk</b> sections used for data partitions based on disk-data.qcow2 (Ex: longhorn)

Note: In the case of <b>DiskPressure</b> errors on kubernetes nodes, check and resize disk-root.qcow2 or disk-data.qcow2.
```
qemu-img resize disk-root.qcow2 +60G
```

## Troubleshooting

You can use the following command to attach to the virtual machine:

```shell
kubectl exec -it libvirt-metal-management-cp-0-0 -- virsh console vbmh
```

And login if you have set passwords for ipa and/or nodes machines.

## Reaching the cluster

As the cluster API & ingresses are exposed on kind container address, it is usually not routed and reachable outside of the host. If you intent to reach the various UIs in the system, one convenient solution is to use ssh SOCKS forwarding:

- run "ssh -D 10555 <username>@<libvirt-metal-host>"
- configure your browser to use SOCKS proxy on localhost:10555, and check the "proxy DNS when using SOCKS option"
- then you will be able to reach `https://rancher.172.19.0.2.nip.io` and other services
