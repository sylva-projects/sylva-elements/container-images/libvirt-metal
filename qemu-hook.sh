#!/bin/sh

# This executes only when a QEMU guest is started and the QEMU process has successfully started up
# (see https://libvirt.org/hooks.html#etc-libvirt-hooks-qemu)
# to have the sole (provisioning) bridge accept packets tagged with vlan 100 coming from the VM

# to find the VM interfaces we search for a modified OUI of the dom.xml MAC address
# while the original MAC address specified in the VM's XML configuration might be globally unique
# (such as 52:54:00:44:44:01), QEMU internally changes it to a locally administered MAC address
# (such as fe:54:00:44:44:01) when creating the virtual network interface for the VM,
# primarily for isolation and to avoid conflicts with physical network interfaces

if [[ $2 == "started" ]]; then
  IFACE1=$(ip -o link show | grep "fe:54:00:44:44" | awk -F': ' '{print $2}')
  IFACE2=$(ip -o link show | grep "fe:54:00:55:55" | awk -F': ' '{print $2}')
  bridge vlan add dev $IFACE1 vid 100
  bridge vlan add dev $IFACE2 vid 100
fi
